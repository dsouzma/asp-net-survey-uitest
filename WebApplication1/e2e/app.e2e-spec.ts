import { browser, element, by } from 'protractor';

describe('AppComponent Tests', () => {

    const EXPECTED_NUMBER_OF_ITEMS: number = null;

    var todoListItems = element.all(by.css('input'));
    browser.ignoreSynchronization = true;
    beforeEach(() => {
        browser.get('/');
        
    });

    it('Browser should have a defined title', (done) => {
        expect(browser.getTitle().toString()).toEqual('Angular Protractor');
    });

    // it('Should get the number of items as defined in item object', (done) => {
    //     todoListItems.count().then(value => {
    //         expect(value).toBe(EXPECTED_NUMBER_OF_ITEMS);
    //     });
    // });

    // it('Should get the first item text as defined', () => {
    //     expect(todoListItems.first().getText().toString()).toEqual('1');

    // });

    // it('Should get the last item text as defined', () => {
    //     expect(todoListItems.last().toString()).toEqual('10');
    // });

    // it('Should select first item', () => {
    //     expect(todoListItems.first().click());
    // });
    
});
