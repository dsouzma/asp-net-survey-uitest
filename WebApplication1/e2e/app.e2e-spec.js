"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
describe('AppComponent Tests', function () {
    var EXPECTED_NUMBER_OF_ITEMS = null;
    var todoListItems = protractor_1.element.all(protractor_1.by.css('input'));
    protractor_1.browser.ignoreSynchronization = true;
    beforeEach(function () {
        protractor_1.browser.get('/');
    });
    it('Browser should have a defined title', function (done) {
        expect(protractor_1.browser.getTitle().toString()).toEqual('Angular Protractor');
    });
    // it('Should get the number of items as defined in item object', (done) => {
    //     todoListItems.count().then(value => {
    //         expect(value).toBe(EXPECTED_NUMBER_OF_ITEMS);
    //     });
    // });
    // it('Should get the first item text as defined', () => {
    //     expect(todoListItems.first().getText().toString()).toEqual('1');
    // });
    // it('Should get the last item text as defined', () => {
    //     expect(todoListItems.last().toString()).toEqual('10');
    // });
    // it('Should select first item', () => {
    //     expect(todoListItems.first().click());
    // });
});
//# sourceMappingURL=app.e2e-spec.js.map