﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeedbackForm.aspx.cs" Inherits="UISurvey.FeedbackForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">  
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width:700px; border:5px solid;">
                <tr>
                    <td style="width:350px; border:5px solid;">
                        <asp:Label runat="server" Text="Highly Dissatisfied"></asp:Label>
                    </td>
                    <td style="width:350px; border:5px solid; text-align: right;">
                        <asp:Label runat="server" Text="Highly Satisfied"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        RepeatDirection="Horizontal"
                         Width="700px"
                         ClientIDMode="Static"
                         >
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                    </asp:RadioButtonList>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

