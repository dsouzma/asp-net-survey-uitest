"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_component_1 = require("./app.component");
var testing_1 = require("@angular/core/testing");
describe('AppComponent', function () {
    var comp;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [app_component_1.AppComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
        comp = fixture.componentInstance;
    });
    it('Should define a list object', function (done) {
        expect(comp.items).toBeDefined();
    });
    //it('Should have 3 items in list', () => {
    //  expect(comp.items.length).toBe(3);
    //});
    //it('List items should be as expected', () => {
    //  expect(comp.items).toEqual(['test','execute','refactor']);
    //});
    //describe('Testing add method', () => {
    //    beforeEach(() => {
    //        comp.add('new-item');
    //    });
    //    it('Should have 4 items in list', () => {
    //        expect(comp.items.length).toBe(4);
    //    });
    //    it('Should add a new item at the end of list', () => {
    //        var lastIndexOfList = comp.items.length - 1;
    //  			expect(comp.items[lastIndexOfList]).toEqual('new-item');
    //    });
    //});
});
//# sourceMappingURL=app.component.spec.js.map