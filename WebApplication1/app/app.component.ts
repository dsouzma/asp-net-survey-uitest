import { Component } from '@angular/core';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';

@Component({
  moduleId: module.id,
   selector: 'my-app',
   templateUrl: 'FeedbackForm.aspx'
})
export class AppComponent {
  items:Array<string>;

  constructor() {
    this.items = ['test','execute','refactor'];
  }

  add(item) {
    this.items.push(item);
  }
};
